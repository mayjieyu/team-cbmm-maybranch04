package SecurityPack;

import DAOs.Article;
import DAOs.ArticleDAO;
import DAOs.Comment;
import DAOs.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

public class AuthenticationModule {

    public AuthenticationModule() {
    }


/*
This is a class used to add additional authentication to our application. The 'check' method
finds the username property stored in the session

 */

    public boolean check(HttpSession session, Object tocheck){

        String username=(String)session.getAttribute("username");
        boolean isVerified=false;

       if (tocheck instanceof DAOs.Article){
         String articleUsername= ((Article) tocheck).getUsername();

         if (articleUsername.equals(username)){
             isVerified=true;
           }


       }

       else if (tocheck instanceof User){
           String userUserName=((User) tocheck).getUsername();

           if (userUserName.equals(username)){
               isVerified=true;
           }

       }


       else if (tocheck instanceof Comment){

           System.out.println("I'm checking!");
           //getting the person who made the comment
           String commentWriterName=((Comment) tocheck).getUsername();
           int articleId=((Comment) tocheck).getArticleId();

           String articleWriter="";

           try(ArticleDAO toAuthenticate=new ArticleDAO()){

               System.out.println("I got the article Dao");

               String articleIDString=String.valueOf(articleId);

             Article toCheckAgainst=toAuthenticate.getArticleByID(articleIDString);

             //getting the writer of the article containing the comment
               articleWriter= toCheckAgainst.getUsername();
               System.out.println("Article writer is "+articleWriter);

           }catch (Exception e){
               System.out.println("An exception occurred");
           }

        //if the user wrote the comment OR wrote the article, he/she has permission to delete.
           if (commentWriterName.equals(username)||articleWriter.equals(username)){
               isVerified=true;
           }else{
               System.out.println("not right!");
           }

       }




        return isVerified;
    }




}
