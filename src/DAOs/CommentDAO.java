package DAOs;



import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommentDAO implements AutoCloseable {
    private final Connection conn;


    public CommentDAO() throws IOException, SQLException {
        this.conn = HikariConnectionPool.getConnection();
    }

    //todo get what we want from the DB by different methods
    public List<Comment> getAllCommentsByArticleId(int id ) throws SQLException {
        List<Comment> comments = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM blog_comments WHERE article_id LIKE ?")) {
                stmt.setInt(1,id);
                stmt.execute();
            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    Comment comment = new Comment();
                    comment.setCommentId(r.getInt("comment_id"));
                    comment.setArticleId(r.getInt("article_id"));
                    comment.setReferenceId(r.getInt("reference_id"));
                    comment.setContent(r.getString("content"));
                    comment.setCreatedDate(r.getString("created_date"));
                    comment.setUsername(r.getString("username"));
                    comments.add(comment);
                }
            } catch (SQLException e) {
                System.out.println("Exception occured");
            }
        }

        return comments;
    }




    public List<Comment> getAllComments( ) throws SQLException {

        List<Comment> comments = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM blog_comments WHERE reference_id IS NULL ")) {


            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    Comment comment = new Comment();
                    comment.setCommentId(r.getInt("comment_id"));
                    comment.setArticleId(r.getInt("article_id"));
                    comment.setReferenceId(r.getInt("reference_id"));
                    comment.setContent(r.getString("content"));
                    comment.setCreatedDate(r.getString("created_date"));
                    comment.setUsername(r.getString("username"));

                    comments.add(comment);
                }
            } catch (SQLException e) {
                System.out.println("Exception occured");
            }
        }

        return comments;
    }


    public void createNewComment(Comment comment){






        try(PreparedStatement stmt = conn.prepareStatement("INSERT INTO blog_comments (article_id, content, username)  VALUES (?,?,?)")) {

                stmt.setInt(1, comment.getArticleId());
                stmt.setString(2, comment.getContent());
                stmt.setString(3, comment.getUsername());



                stmt.execute();

        } catch (SQLException e) {
            System.out.println("Exception occured");
            System.out.println("In create new comment");
        }
    }



    public Comment getCommentById (int id){
            Comment toReturn=null;
        System.out.println("get commentbyID");

        try(PreparedStatement stmt = conn.prepareStatement("SELECT  * FROM  blog_comments WHERE ( comment_id) LIKE ?")) {
            stmt.setInt(1, id);
            stmt.execute();

            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    toReturn= new Comment();
                    toReturn.setCommentId(r.getInt("comment_id"));
                    toReturn.setArticleId(r.getInt("article_id"));
                    toReturn.setReferenceId(r.getInt("reference_id"));
                    toReturn.setContent(r.getString("content"));
                    toReturn.setCreatedDate(r.getString("created_date"));
                    toReturn.setUsername(r.getString("username"));


                }

            }

        } catch (SQLException e) {
            System.out.println("Exception occured");
            System.out.println("in nested comment");
        }


        return toReturn;
    }


    public void createNestedComment(Comment nestcomment){
        //to create new nestedCommentTable, to connect nestedCommentId with articleId already created to


        try(PreparedStatement stmt = conn.prepareStatement("INSERT INTO blog_comments ( article_id, content, username, reference_id)  VALUES (?,?,?,?)")) {


            stmt.setInt(1, nestcomment.getArticleId());
            stmt.setString(2, nestcomment.getContent());
            stmt.setString(3, nestcomment.getUsername());
            stmt.setInt(4,nestcomment.getReferenceId());
            stmt.execute();

        } catch (SQLException e) {
            System.out.println("Exception occured");
            System.out.println("in nested comment");
        }
    }


public List<Comment> getNestedComments() {

    List<Comment> comments = new ArrayList<>();

    try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM blog_comments WHERE reference_id IS NOT NULL ")) {

        try (ResultSet r = stmt.executeQuery()) {
            while (r.next()) {
                Comment comment = new Comment();
                comment.setCommentId(r.getInt("comment_id"));
                comment.setArticleId(r.getInt("article_id"));
                comment.setReferenceId(r.getInt("reference_id"));
                comment.setContent(r.getString("content"));
                comment.setCreatedDate(r.getString("created_date"));
                comment.setUsername(r.getString("username"));
                System.out.println(comment.getReferenceId());
                comments.add(comment);
            }

        }

    } catch (SQLException e) {
        System.out.println("Exception occured");
        System.out.println("in nested comment");
        System.out.println(""+e.getMessage());
    }

    return comments;
}


    public  void deleteComment(int id) throws SQLException {

        try( PreparedStatement stmt = conn.prepareStatement("DELETE FROM blog_comments WHERE blog_comments.comment_id LIKE ?")) {

                stmt.setInt(1, id);
                stmt.execute();



        } catch (SQLException e) {
            System.out.println("Exception occured");
        }

    }


    public  void deleteAllComments(String username) throws SQLException {

        try( PreparedStatement stmt = conn.prepareStatement("DELETE FROM blog_comments WHERE username LIKE ?")) {

            stmt.setString(1, username);
            stmt.execute();



        } catch (SQLException e) {
            System.out.println("Exception occured");
        }

    }






    @Override
    public void close() throws Exception {
        this.conn.close();
    }
}
