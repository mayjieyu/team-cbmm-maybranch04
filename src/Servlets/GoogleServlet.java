package Servlets;

import DAOs.User;
import DAOs.UserDAO;
import Recaptcha.VerifyRecaptcha;
import org.owasp.encoder.Encode;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class GoogleServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //if never login, create an account, else login
        String username = request.getParameter("username");
        boolean isTaken = false;
        try (UserDAO userDAO = new UserDAO()) {
            isTaken = userDAO.isTaken(username);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //create an new account
        if (!isTaken) {
            User newUser = new User();
            newUser.setUsername(username);
            newUser.setFname(request.getParameter("fname"));
            newUser.setLname(request.getParameter("fname"));
            newUser.setLname(request.getParameter("lname"));
            newUser.setEmail(request.getParameter("email"));
            String avatar = "avatar_default.png"; //use the default img as the avatar
            try (UserDAO userDAO = new UserDAO()) {
                userDAO.createNewUser(newUser);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        request.getSession().setAttribute("username", username);
        request.getSession().setAttribute("source", "facebook");
        response.sendRedirect("Articles");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
