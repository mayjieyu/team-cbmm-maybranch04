<div class="modal fade" id="articleModal" tabindex="-1" role="dialog" aria-labelledby="modal_title"
     aria-hidden="true" style="width: 96%; height: 100%; margin-left: 2%; margin-right: 2%">


    <div class="modal-dialog" style="width: 100%" role="document">
        <div class="modal-content row" style="width: 100%">


            <div class="modal-body col-md-8">

                <h3 class="modal_title"></h3>
                <h5 class="modal_user"></h5>
                <p id="modal_id"></p>
                <%--&lt;%&ndash;<h5 class="modal_user"> An UserName here</h5>&ndash;%&gt;--%>
                <%--<p id="modal_id"></p>--%>

                <%--<p class="modal_title"> Hello Sunshine</p>--%>

                <p class="modal_txt"></p>
                <%--<p class="modal_txt">Есть много вариантов, но большинство из них имеет не всегда приемлемые--%>
                <%--модификации, например, юмористические--%>
                <%--вставки или слова, которые даже отдалённо не напоминают латынь.--%>
                <%--Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки,--%>
                <%--скрытой в середине абзаца.</p>--%>

            </div>
            <div class="col-md-3">
                <div id="commentArea" style="background-color: #e0e3e4">
                    <c:choose>
                        <c:when test="${fn:length(comments) gt 0}">
                            <c:forEach items="${comments}" var="comment">

                                <%--todo--%>
                                <div class="commentArticleId">${comment.articleId}</div>
                                <div class="comment" data-id="${comment.articleId}"> ${comment.content}
                                    <button data-toggle="modal" data-target="#nestedModalz"
                                            onclick="testfunction(${comment.commentId})" class="smallClick">
                                        <img class="cmtImgz" src="../image/comment.png">

                                    </button>
                                    <button type="button" class="deleteButtonz" id="deleteButton"
                                            onclick="deleteComment(${comment.commentId})"
                                    ><img class="dltCmtImgz" src="../image/delete.png">
                                    </button>

                                    <c:set var="timeString" value="${comment.createdDate}"/>
                                    <div><p class="commentStamp"> left by: ${comment.username}
                                        at ${fn:substring(timeString, 5, 16)} </p></div>
                                </div>
                                <%--todo--%>
                                <div class="commentArticleId">${comment.articleId}</div>

                                <%--<c:choose>--%>
                                <%--<c:when test="${username == comment.username}">--%>


                                <%--</c:when>--%>
                                <%--<c:otherwise> </c:otherwise>--%>
                                <%--&lt;%&ndash;</c:if>&ndash;%&gt;--%>
                                <%--</c:choose>--%>


                                <c:choose>
                                    <c:when test="${fn:length(nestedComments) gt 0}">
                                        <c:forEach var="ncomment" items="${nestedComments}">
                                            <c:choose>
                                                <c:when test="${ncomment.referenceId==comment.commentId}">
                                                    <div class="tinyComments">${ncomment.content}
                                                        <c:set var="timeString" value="${ncomment.createdDate}"/>
                                                        <div><p class="commentStamp"> left by: ${ncomment.username}
                                                            at ${fn:substring(timeString, 5, 16)} </p></div>
                                                    </div>
                                                    <div class="tinyID" hidden> ${ncomment.articleId}</div>
                                                </c:when>
                                                <c:otherwise> </c:otherwise>
                                            </c:choose>

                                        </c:forEach>


                                    </c:when>
                                    <c:otherwise><p></p></c:otherwise>
                                </c:choose>


                            </c:forEach>
                        </c:when>
                        <c:otherwise><p> No comments here!</p></c:otherwise>

                    </c:choose>

                    <%--<c:choose>--%>
                    <%--<c:when test="${fn:length(nestedComments) gt 0}">--%>

                    <%--</c:when>--%>


                    <%--<c:otherwise><p> no nested comments</p></c:otherwise>--%>
                    <%--</c:choose>--%>
                    <%--</c:choose>--%>
                    <%--<c:otherwise> <p></p></c:otherwise>


    <%----%>
                </div>

                <div>
                    <form method="post" action="Articles" id="articleForm">

                        <textarea rows="2" cols="50" id="commentTextArea"></textarea>
                        <button type="button" class="btn btn-primary" data-toggle="modal" onclick="leaveComment()"
                                id="commentButton">Comment
                        </button>
                        <%--no value of id parse to servlet "article"--%>
                        <input type="text" id="editID" name="editID">
                        <%--<input type="text" id="articleID" name="articleID" readonly>--%>
                        <input type="text" id="editTitle" name="editTitle" required>
                        <span style="color: red">*</span>
                        <textarea id="editTextArea" name="editTextArea"></textarea>
                        <button type="submit" class="btn btn-primary save" data-toggle="modal" onclick="editCookie()"
                                id="saveButton">Save
                        </button>


                        <%--<textarea rows="2" cols="50" id="commentTextArea"></textarea>--%>
                        <%--<button type="button" class="btn btn-primary" data-toggle="modal" onclick="leaveComment()"--%>
                        <%--id="commentButton">Comment--%>
                        <%--</button>--%>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>


                <button id="editButton" type="button" class="btn btn-primary edit" data-toggle="modal"
                        onclick="displayEdit()">Edit
                    Article
                </button>
            </div>
        </div>
    </div>
</div>