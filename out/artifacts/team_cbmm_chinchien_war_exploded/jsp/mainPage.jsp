<%@ page import="java.util.List" %>
<%@ page import="DAOs.Article" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.ParseException" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Main Page</title>

    <%@include file="bcss.jsp"%>
    <%--bootstrap--%>

    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <%--<script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>

    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--%>
    <%--css--%>
    <%--<link rel="stylesheet" type="text/css" href="<c:url value='../css/mainPage.css' />"/>--%>
    <%--summernote--%>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

    <%--my css--%>
    <link rel="stylesheet" type="text/css" href="../css/mainPage.css"/>
    <link rel="stylesheet" type="text/css" href="../css/website.css"/>


</head>

<body class="bodyStyle">

<header>
    <%@ include file="navbar.jsp" %>
</header>

<div class="container">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">

            <div class="item active">
                <img src="image/image01.png" alt="Technology" style="width:100%;">
                <div class="carousel-caption">
                    <h3>Technology</h3>
                    <p>Technology is connecting the World at a speed of 'speed of 'light' '</p>
                </div>
            </div>

            <div class="item">
                <img src="image/image04.png" alt="Health" style="width:100%;">
                <div class="carousel-caption">
                    <h3>Health</h3>
                    <p>Health is Wealth!</p>
                </div>
            </div>

            <div class="item">
                <img src="image/image03.png" alt="Education" style="width:100%;">
                <div class="carousel-caption">
                    <h3>Education</h3>
                    <p>Learning new things 'FastPace' is like taking an daily dose of Adrenaline!!</p>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="container">
    <div class="tab1">
        <%--<div class="col-md-12 row">--%>
            <div class="col-md-12 row getRidOfpad">
                <form  class="col-md-4 getRidOfpad" action="SearchServlet" method="post">
                    <div class="col-md-12">
                        <input class="col-md-9 searchArticle" name="command" type="text" placeholder="Search article or author.." class="allInput" value="${command}" />
                        <input class="col-md-2 col-md-offset-1 btn btn-dange searchButton" type="submit" class="tabSub" value="Search"/>
                    </div>
                </form>
                <form class="col-md-4 getRidOfpad" action="SearchServlet" method="post">
                    <div class="col-md-12">
                        <input class="col-md-9 searchDate" type="date" name="date" value="${date}">
                        <input class="col-md-2 col-md-offset-1 btn  searchButton" type="submit">
                    </div>
                </form>
                <form  class="col-md-4 getRidOfpad" action="SearchServlet" method="post">
                    <div class="col-md-12">
                        <input class='col-md-5 searchButton' type="submit" name="sortForward" value="Ascending order">
                        <input class='col-md-5 searchButton' type="submit" name="sortBackward" value="Descending order">
                    </div>
                </form>
            </div>


            <%--<tr class="col-md-12">--%>
                <%--&lt;%&ndash;<td width="90" align="right">Search</td>&ndash;%&gt;--%>
                <%--<td>--%>
                    <%----%>
                <%--</td>--%>
                <%--<td width="85" align="right"><input type="submit" class="tabSub" value="Search"/></td>--%>
            <%--</tr>--%>
            <%--<br>--%>
            <%--<tr class="col-md-12">--%>
                <%--<input class='col-md-2' type="date" name="date" value="${date}">--%>
                <%--<input class='col-md-3' type="submit">--%>
            <%--</tr>--%>
        <%--</form>--%>
        <%--<div class="col-md-6 col-md-offset-9">--%>
        <%--<div class="col-md-12 row">--%>
            <%--<form  class="col-md-6 col-md-offset-6" action="SearchServlet" method="post">--%>
                <%--<input class='col-md-3 searchButton' type="submit" name="sortForward" value="sortForward">--%>
                <%--<input class='col-md-3 searchButton' type="submit" name="sortBackward" value="sortBackward">--%>
            <%--</form>--%>
        <%--</div>--%>

        <%--</div>--%>

    </div>
</div>


<div class="">


    <div class="container container-margin">
        <c:choose>
            <c:when test="${fn:length(articles) gt 0}">

                <c:forEach var="article" items="${articles}">


                    <%--<h3 class="panel-title pull-left"><a href="?article=${article.id}"></a></h3>--%>

                    <div class="row contentCard02 col-md-12" id="${article.articleId}">
                        <div class="">
                            <div class="innerContent">
                                <h5 class="card-title"> ${article.title}</h5>
                                <p class="author">By / <span>${article.username}</span>
                                    <span>${article.modifiedDateAndTime}</span></p>

                                    <%--only show short content with 200 chars--%>
                                    <%--<hr class="line">--%>
                                <div class="articleContent">
                                    <p>${fn:substring(article.content, 0, 199)}</p>
                                </div>



                                <button type="button" class="btn load btn-set col-md-6" data-toggle="modal"
                                        data-target="#articleModal"
                                        data-username="${article.username}" data-content="${article.content}"
                                        data-title="${article.title}"
                                        data-id="${article.articleId}"   onclick="displayNestedComments(${article.articleId}); showDelete( ${username})">
                                    <img src="image/read.png" alt="Technology" style="width:20px;">
                                    Load Article
                                </button>
                                    <%--Chinchien view blog--%>
                                <button class="btn load btn-set col-md-6" onclick="location.href='HomePage?status=${article.username}';"
                                        value="View Blog">
                                    <img src="image/read.png" alt="Technology" style="width:20px;">
                                        View Blog</button>
                            </div>

                                <%--<p class="card-text">${article.content}</p>--%>
                                <%--<a href="#" class="btn btn-primary">Load Full Article</a>--%>


                                <%--<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#articleModal"   >--%>
                                <%--&lt;%&ndash;data-content="${article.content}" data-title="${article.title}"    &ndash;%&gt;--%>
                                <%--Load Article--%>
                                <%--</button>--%>
                        </div>
                            <%--<button id = "load" type="button" class="btn btn-danger load" data-toggle="modal" data-target="#articleModal"--%>
                            <%--data-username="${article.username}" data-content="${article.content}" data-title="${article.title}"--%>
                            <%--data-id="${article.articleId}">--%>
                            <%--Load Article--%>
                            <%--</button>--%>

                        <c:choose>
                            <c:when test="${username == article.username}">
                                <button type="button" id="deleteButton" class="btn load col-md-1 btn-delete col-md-offset-11"
                                        onclick="deleteArticle(${article.articleId})"
                                        data-articleID="">
                                    <img src="image/trashbin.png" alt="Technology" style="width:20px;">
                                    delete
                                </button>


                            </c:when>
                            <c:otherwise>
                            </c:otherwise>
                        </c:choose>
                    </div>

                </c:forEach>
            </c:when>
            <c:otherwise>
                <p>No articles!</p>
            </c:otherwise>
        </c:choose>
        <br>

    </div>
</div>

<%--<script type="text/javascript" src="../js/jquery.min.js"></script>--%>
<%--<script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>


<%@include file="articleModal.jsp" %>
<%@include file="scripts.jsp" %>
<%@include file="nestedCommentModal.jsp" %>
<%--<%@include file="editmodal.jsp"%>--%>

<script>
    $(document).ready(function () {
        $('.summernote').summernote({
            height: 300
            // focus: true
        });
        // $('.dropdown-toggle').dropdown();
    });
</script>
</body>
</html>
